# למכירה: דירת נופש במרכז צ'רנוביץ, אוקראינה

3 חדרים גדולים (77 מ"מ), מרפסת סוכה, קומה 2. מיקום מרכזי, קרוב לבית כנסת ומסעדה כשרה.  
מתאים כנדל"ן עסקי: כדירת נופש ולטיולי שורשים. קרוב להרי הקרפטים וגבול רומניה. יש בעיר שדה תעופה. סמוך **לסדיגורה, בויאן**.  
קרוב **לויז'ניץ, סטרוז'נץ, נדברנה, שאץ, סערט, סוליצא‎, שטפנשט, הורודנקה, זלישטשיק, הביזדץ, זבלטוב, קוסוב, אוטיניא, צ'ורטקוב, הוסיאטין, וכו'**.  

מיקום מדויק בגוגל מאפס:  
https://goo.gl/maps/rf8dsmwYakC2

מחיר: **$80,000** (כולל עזרה בהקמת עסק).  
לפרטים: 058-324-7556 (972+)  
www.bit.ly/NaDLaN

# למכירה: חלקת אדמה עם בית כפרי ישן

חלקה: 40 על 40 מטר. בית: 5 על 10 מטר (2 חדרים).  
עם [תנור רוסי](https://en.wikipedia.org/wiki/Russian_oven) אותנטי. נוף פסטורלי.  
מתאים לבנית בתי הארחה או וילת נופש (פרטי או עסקי). בגבול אוקראינה ורומניה.  
לא רחוק **מצ'רנוביץ, סדיגורה, בויאן, סטרוז'נץ, שטפנשט, צ'ורטקוב**.   

מיקום מדויק בגוגל מאפס:  
https://goo.gl/maps/EMvK4BW5yYE2

מחיר: **$10,000**  
לפרטים: 058-324-7556 (972+)  
www.bit.ly/NaDLaN

# שני הנכסים במחיר הזדמנות $87,500!